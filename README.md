[![GitHub issues](https://img.shields.io/github/issues/jamesgeorge007/Crypto-App.svg)](https://github.com/jamesgeorge007/Crypto-App/issues)  [![GitHub forks](https://img.shields.io/github/forks/jamesgeorge007/Crypto-App.svg)](https://github.com/jamesgeorge007/Crypto-App/network)  [![GitHub stars](https://img.shields.io/github/stars/jamesgeorge007/Crypto-App.svg)](https://github.com/jamesgeorge007/Crypto-App/stargazers) [![GitHub license](https://img.shields.io/github/license/jamesgeorge007/Crypto-App.svg)](https://github.com/jamesgeorge007/Crypto-App/blob/master/LICENSE)

<h1 align="center"> Crypto-App </h1>

This is a Desktop application built in Electron JS which notifies the user whenever value of Bitcoin goes above a certain value specified by the user in USD.

## Preview

![Image](https://github.com/jamesgeorge007/Crypto-App-in-Electron-JS/blob/master/src/assets/preview/main_window.JPG)

# License

> **The MIT License**

